<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not give, sell, distribute, sub-license, rent, lease or lend
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category  ETWS
 * @package   ETWS_ReviewNotify
 * @copyright Copyright (c) 2017 ET Web Solutions (http://etwebsolutions.com)
 * @contacts  support@etwebsolutions.com
 * @license  https://shop.etwebsolutions.com/etws-license-free-v1/   ETWS Free License (EFL1)
 */

namespace ETWS\ReviewNotify\Controller\Product;


/**
 * Class Prepost
 * @package ETWS\ReviewNotify\Controller\Review\Product
 */
class Prepost extends ReviewControllerAbstract
{
    /**
     * Submit Action PrePost ((json response), calc unique 'key' for review form from post data)
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */

    /**
     * @return \Magento\Framework\App\Response\Http
     */
    public function execute()
    {
        /** @var \Magento\Framework\App\Request\Http $request */
        $request = $this->getRequest();
        /** @var \Zend\Stdlib\Parameters $post */
        $post = $request->getPost();

        $postHash = $this->calculateHash($post);

        /** @var \Magento\Framework\App\Response\Http $response */
        $response = $this->getResponse();
        $response->clearHeaders()->setHeader('Content-type', 'application/json', true);

        $response->setNoCacheHeaders();
        
        $responseData = array(
            'sequence' => $postHash
        );

        $response->setContent(json_encode($responseData));

        return $response;
    }

}


