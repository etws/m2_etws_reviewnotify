<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not give, sell, distribute, sub-license, rent, lease or lend
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category  ETWS
 * @package   ETWS_ReviewNotify
 * @copyright Copyright (c) 2017 ET Web Solutions (http://etwebsolutions.com)
 * @contacts  support@etwebsolutions.com
 * @license  https://shop.etwebsolutions.com/etws-license-free-v1/   ETWS Free License (EFL1)
 */

namespace ETWS\ReviewNotify\Controller\Product;

use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Design;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Framework\Registry;
use Magento\Framework\Session\Generic;
use Magento\Review\Controller\Product\Post as ReviewPost;
use Magento\Review\Model\RatingFactory;
use Magento\Review\Model\ReviewFactory;
use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface;

/**
 * Class ReviewControllerAbstract
 * @package ETWS\ReviewNotify\Controller\Product
 */
class ReviewControllerAbstract extends ReviewPost
{
    const SECRET_KEY = "Magento_Review_Special_Codes_String";

    protected $scopeConfig;

    /**
     * @inheritdoc
     *
     * ReviewControllerAbstract constructor.
     * @param Context $context
     * @param Registry $coreRegistry
     * @param Session $customerSession
     * @param CategoryRepositoryInterface $categoryRepository
     * @param LoggerInterface $logger
     * @param ProductRepositoryInterface $productRepository
     * @param ReviewFactory $reviewFactory
     * @param RatingFactory $ratingFactory
     * @param Design $catalogDesign
     * @param Generic $reviewSession
     * @param StoreManagerInterface $storeManager
     * @param Validator $formKeyValidator
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        Session $customerSession,
        CategoryRepositoryInterface $categoryRepository,
        LoggerInterface $logger,
        ProductRepositoryInterface $productRepository,
        ReviewFactory $reviewFactory,
        RatingFactory $ratingFactory,
        Design $catalogDesign,
        Generic $reviewSession,
        StoreManagerInterface $storeManager,
        Validator $formKeyValidator,
        ScopeConfigInterface $scopeConfig
    )
    {
        $this->scopeConfig = $scopeConfig;
        parent::__construct(
            $context, $coreRegistry, $customerSession, $categoryRepository, $logger, $productRepository,
            $reviewFactory, $ratingFactory, $catalogDesign, $reviewSession, $storeManager, $formKeyValidator
        );
    }

    /**
     * Calculate hash (md5) from data
     *
     * @param $data
     * @return string
     */
    protected function calculateHash($data)
    {
        $allKeys = array("title", "nickname", "detail");
        $parts = array(self::SECRET_KEY);
        foreach ($allKeys as $key) {
            $parts[] = isset($data[$key]) ? $data[$key] : rand();
        }

        return md5(implode("|", $parts));
    }

}