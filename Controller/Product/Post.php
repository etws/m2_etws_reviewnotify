<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not give, sell, distribute, sub-license, rent, lease or lend
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category  ETWS
 * @package   ETWS_ReviewNotify
 * @copyright Copyright (c) 2017 ET Web Solutions (http://etwebsolutions.com)
 * @contacts  support@etwebsolutions.com
 * @license  https://shop.etwebsolutions.com/etws-license-free-v1/   ETWS Free License (EFL1)
 */

namespace ETWS\ReviewNotify\Controller\Product;

use Magento\Framework\Controller\ResultFactory;

/**
 * Class Post
 * @package ETWS\ReviewNotify\Controller\Product
 */
class Post extends ReviewControllerAbstract
{
    /**
     * override,
     * check parameter 'sequence' in post & validate with post data
     *
     * @inheritdoc
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        $antiSpamEnabled = (bool)$this->scopeConfig
            ->getValue('catalog/review/etwsrn_antispam_enabled');
        
        if (!$antiSpamEnabled) {
            return parent::execute();
        }

        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        if (!$this->formKeyValidator->validate($this->getRequest())) {
            $resultRedirect->setUrl($this->_redirect->getRefererUrl());
            return $resultRedirect;
        }

        /** @var \Magento\Framework\App\Request\Http $request */
        $request = $this->getRequest();
        /** @var \Zend\Stdlib\Parameters $post */
        $post = $request->getPost();

        $sequence = $post->get('sequence');

        if (strcmp($sequence, $this->calculateHash($post)) != 0) {
            $this->messageManager->addErrorMessage(__('Unable to post the review.'));
            /** @noinspection PhpUndefinedMethodInspection */
            $redirectUrl = $this->reviewSession->getRedirectUrl(true);
            $resultRedirect->setUrl($redirectUrl ?: $this->_redirect->getRedirectUrl());
            return $resultRedirect;
        }
        
        return parent::execute();
    }
}


