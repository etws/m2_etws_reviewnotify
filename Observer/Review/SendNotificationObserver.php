<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not give, sell, distribute, sub-license, rent, lease or lend
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category  ETWS
 * @package   ETWS_ReviewNotify
 * @copyright Copyright (c) 2017 ET Web Solutions (http://etwebsolutions.com)
 * @contacts  support@etwebsolutions.com
 * @license  https://shop.etwebsolutions.com/etws-license-free-v1/   ETWS Free License (EFL1)
 */

namespace ETWS\ReviewNotify\Observer\Review;

use ETWS\ReviewNotify\Helper\Data as EtwsHelper;
use Magento\Backend\Helper\Data as BackendHelper;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\Framework\App\Area;
use Magento\Framework\Event;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Review\Model\Review;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class SendNotificationObserver
 * @package ETWS\ReviewNotify\Observer\Review
 * @author Vadims Bucinskis <v.buchinsky@etwebsolutions.com>
 */
class SendNotificationObserver implements ObserverInterface
{
    /** @var ProductRepositoryInterface */
    protected $productRepository;
    /** @var TransportBuilder */
    protected $transportBuilder;
    /** @var StoreManagerInterface */
    protected $storeManager;
    /** @var BackendHelper */
    protected $helperBackend;
    /** @var EtwsHelper */
    protected $etwsHelper;
    /** @var TimezoneInterface */
    protected $localeDate;
    /** @var array */
    protected $transportData;
    /** @var Review */
    protected $review;


    /**
     * SendNotificationObserver constructor.
     *
     * @param ProductRepositoryInterface $productRepository
     * @param TransportBuilder $transportBuilder
     * @param StoreManagerInterface $storeManager
     * @param TimezoneInterface $localeDate
     * @param EtwsHelper $etwsHelper
     * @param BackendHelper $helperBackend
     */
    public function __construct(
        ProductRepositoryInterface $productRepository,
        TransportBuilder $transportBuilder,
        StoreManagerInterface $storeManager,
        TimezoneInterface $localeDate,
        EtwsHelper $etwsHelper,
        BackendHelper $helperBackend
    )
    {
        $this->localeDate = $localeDate;
        $this->productRepository = $productRepository;
        $this->transportBuilder = $transportBuilder;
        $this->storeManager = $storeManager;
        $this->helperBackend = $helperBackend;
        $this->etwsHelper = $etwsHelper;
    }

    /**
     * Send notification email about new order if needed
     *
     * @inheritdoc
     *
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        /** @var Event $event */
        $event = $observer->getEvent();
        /** @var Review $review */
        $review = $event->getData('object');

        $helper = $this->etwsHelper;
        if (!$helper->isNeedSendNotification($review->getData('store_id'))) {
            $helper->log('Notifications are disabled in the extension settings');
            return true;
        }

        try {
            if (!($review instanceof Review)) {
                throw new \Exception('Unable to process review because it does not extend Magento\Review\Model\Review');
            }
            $this->review = $review;

            $data = $this->getTransportData();
            foreach ($this->getRecipientEmails() as $recipientEmail) {
                try {
                    /** @var \Magento\Framework\Mail\Transport $transport */
                    $transport = $this->transportBuilder
                        ->setTemplateIdentifier($data['email_template_id'])
                        ->setTemplateOptions($data['template_option'])
                        ->setTemplateVars($data['template_vars'])
                        ->setFrom($data['email_from'])
                        ->addTo($recipientEmail)
                        ->getTransport();

                    $transport->sendMessage();
                } catch (\Exception $ex) {
                    $helper->log($ex->getMessage());
                    $helper->log('Notification was not sent');
                }
            }

        } catch (\Exception $e) {
            $helper->log($e->getMessage());
            $helper->log('Notification was not sent');
        }
        return true;
    }

    /**
     * Load and retrieve transport data for email
     *
     * @return array
     */
    protected function getTransportData()
    {
        if (empty($this->transportData)) {
            $this->getEmailTemplateId();
            $this->getFromEmail();
            $this->getTemplateVars();

            $this->transportData['template_option'] = [
                'store' => $this->review->getData('store_id'),
                'area' => Area::AREA_FRONTEND
            ];
        }
        return $this->transportData;
    }

    /**
     * Load email template ID
     *
     * @return int|string
     * @throws \Exception
     */
    protected function getEmailTemplateId()
    {
        if (!isset($this->transportData['email_template_id'])) {
            $emailTemplateId = $this->etwsHelper->getNotificationEmailTemplateId($this->review->getData('store_id'));
            if (!$emailTemplateId) {
                throw new \Exception('Notification template is not specified');
            }
            $this->transportData['email_template_id'] = $emailTemplateId;
        }
        return $this->transportData['email_template_id'];
    }

    /**
     * Load email address to send notification from
     *
     * @return array
     * @throws \Exception
     */
    protected function getFromEmail()
    {
        if (!isset($this->transportData['email_from'])) {
            $emailFrom = $this->etwsHelper->getNotificationFromEmail($this->review->getData('store_id'));
            if (!$emailFrom) {
                throw new \Exception('Sender identity is not specified');
            }
            $this->transportData['email_from'] = $emailFrom;
        }
        return $this->transportData['email_from'];
    }

    /**
     * Load necessary template variables
     *
     * @return array
     */
    protected function getTemplateVars()
    {
        if (!isset($this->transportData['template_vars'])) {
            $review = $this->review;
            $date = new \DateTime($review->getData('created_at'));
            $locale = $this->etwsHelper->getConfig('general/locale/code', $review->getData('store_id'));
            $timezone = $this->etwsHelper->getConfig(
                $this->localeDate->getDefaultTimezonePath(),
                $review->getData('store_id')
            );
            $dateStr = $this->localeDate->formatDateTime(
                $date,
                \IntlDateFormatter::MEDIUM,
                \IntlDateFormatter::MEDIUM,
                $locale,
                $timezone
            );

            /** @var Product $product */
            $product = $this->productRepository->getById($review->getEntityPkValue());

            $templateVars = array(
                'product' => $product->getName() . ' (sku: ' . $product->getSku() . ')',
                'title' => $review->getData('title'),
                'nickname' => $review->getData('nickname'),
                'details' => $review->getData('detail'),
                'id' => $review->getId(),
                'date' => $dateStr,
                'admin_url' => $this->helperBackend->getHomePageUrl()
            );
            $this->transportData['template_vars'] = $templateVars;
        }
        return $this->transportData['template_vars'];
    }

    /**
     * Retrieve recipient email addresses
     *
     * @return array|false
     * @throws \Exception
     */
    protected function getRecipientEmails()
    {
        $recipientEmails = $this->etwsHelper->getNotificationRecipientEmails($this->review->getData('store_id'));
        if (empty($recipientEmails)) {
            throw new \Exception('There is no valid email addresses to send notification to');
        }
        return $recipientEmails;
    }
}