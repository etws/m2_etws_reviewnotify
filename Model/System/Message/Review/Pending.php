<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not give, sell, distribute, sub-license, rent, lease or lend
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category  ETWS
 * @package   ETWS_ReviewNotify
 * @copyright Copyright (c) 2017 ET Web Solutions (http://etwebsolutions.com)
 * @contacts  support@etwebsolutions.com
 * @license  https://shop.etwebsolutions.com/etws-license-free-v1/   ETWS Free License (EFL1)
 */

namespace ETWS\ReviewNotify\Model\System\Message\Review;

use ETWS\ReviewNotify\Helper\Data as EtwsHelper;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Notification\MessageInterface;
use Magento\Framework\UrlInterface;
use Magento\Review\Model\ResourceModel\Review\Collection;
use Magento\Review\Model\ResourceModel\Review\CollectionFactory;
use Magento\Review\Model\Review;

/**
 * Class Pending
 * @package ETWS\ReviewNotify\Model\System\Message\Review
 * @author Sergey Nosirev <sergey.nosirev@etwebsolutions.com>
 * @author Vadims Bucinskis <v.buchinsky@etwebsolutions.com>
 */
class Pending implements MessageInterface
{
    /** @var CollectionFactory $ratingFactory */
    protected $ratingFactory;
    /** @var UrlInterface */
    protected $urlBuilder;
    /** @var Collection $reviewCollection */
    protected $reviewCollection;
    /** @var ProductRepositoryInterface */
    protected $productRepository;
    /** @var EtwsHelper */
    protected $etwsHelper;

    /**
     * @inheritdoc
     *
     * Notification constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param CollectionFactory $ratingFactory
     */
    public function __construct(
        CollectionFactory $ratingFactory,
        UrlInterface $urlBuilder,
        ProductRepositoryInterface $productRepository,
        EtwsHelper $etwsHelper
    )
    {
        $this->ratingFactory = $ratingFactory;
        $this->urlBuilder = $urlBuilder;
        $this->productRepository = $productRepository;
        $this->etwsHelper = $etwsHelper;
    }

    /**
     * @inheritdoc
     * @return string
     */
    public function getIdentity()
    {
        return md5('ETWS_REVIEW_PENDING');
    }

    /**
     * check if:
     * - notifications are enabled and
     * - is review status = pending
     *
     * @inheritdoc
     *
     * @return bool
     */
    public function isDisplayed()
    {
        $showNotification = $this->etwsHelper->isNeedShowNotification();
        if ($showNotification) {
            $collection = $this->getReviewCollection();
            if ($collection->getSize()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get pending review collection
     *
     * @return Collection
     */
    protected function getReviewCollection()
    {
        if (!$this->reviewCollection) {
            $this->reviewCollection = $this->ratingFactory->create();
            $this->reviewCollection->addStoreData();
            $this->reviewCollection->addFieldToFilter(
                'status_id',
                array('eq' => Review::STATUS_PENDING)
            );
        }
        return $this->reviewCollection;
    }

    /**
     * Build text for notification in admin
     *
     * @inheritdoc
     * @return \Magento\Framework\Phrase
     */
    public function getText()
    {
        $msg = '';
        $collection = $this->getReviewCollection();
        $reviewCount = $collection->getSize();

        $urlBuilder = $this->urlBuilder;
        if ($reviewCount > 1) {
            $msg = __(
                'There are %1 reviews waiting to be approved. Click here to see the %2.',
                $reviewCount,
                '<a href="' . $urlBuilder->getUrl('review/product') . '">' . __('Pending Reviews') . '</a>'
            );
        } elseif ($reviewCount == 1) {
            /** @var \Magento\Review\Model\Review $review */
            $review = $collection->getFirstItem();
            $product = $this->productRepository->getById($review->getEntityPkValue());

            $productHref = $urlBuilder->getUrl('catalog/product/edit/', array('id' => $product->getId()));
            $reviewHref = $urlBuilder->getUrl('review/product/edit/', array('id' => $review->getId()));
            /** @noinspection PhpUndefinedMethodInspection */
            $msg = __(
                'There is a review of %1 waiting to be approved. Click here to read %2.',
                '<a href="' . $productHref . '">' . $product->getName() . '</a>',
                '<a href="' . $reviewHref . '">' . $review->getTitle() . '</a>'
            );
        }

        return $msg;
    }

    /**
     * Retrieve message severity
     *
     * Possible values: SEVERITY_CRITICAL, SEVERITY_MAJOR, SEVERITY_MINOR, SEVERITY_NOTICE
     *
     * @return int
     */
    public function getSeverity()
    {
        return self::SEVERITY_MAJOR;
    }
}