== Extension links

Permanent link: https://shop.etwebsolutions.com/eng/m2-reviewnotify.html
Support link: http://support.etwebsolutions.com/projects/m2-reviewnotify/issues/new

== Short description
The extension notifies the owner of a Magento 2 website about new and pending reviews, 
and complicates things for spam-bots posting automatic reviews.

== Version Compatibility
Magento CE: 
2.1.x (tested in 2.1.3)

== Installation
http://doc.etwebsolutions.com/en/instruction/m2-reviewnotify/install

== Information for Developers
http://doc.etwebsolutions.com/en/instruction/m2-reviewnotify/technical-information-for-the-developer
