<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not give, sell, distribute, sub-license, rent, lease or lend
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category  ETWS
 * @package   ETWS_ReviewNotify
 * @copyright Copyright (c) 2017 ET Web Solutions (http://etwebsolutions.com)
 * @contacts  support@etwebsolutions.com
 * @license  https://shop.etwebsolutions.com/etws-license-free-v1/   ETWS Free License (EFL1)
 */

namespace ETWS\ReviewNotify\Helper;

use ETWS\Base\Helper\AbstractHelper;

/**
 * Class Data
 *
 * @package ETWS\ReviewNotify\Helper
 * @author Vadims Bucinskis <v.buchinsky@etwebsolutions.com>
 */
class Data extends AbstractHelper
{
    /**
     * Check if email notifications are enabled
     *
     * @param int|null $storeId
     * @return bool
     */
    public function isNeedSendNotification($storeId = null)
    {
        return (bool)$this->getConfig('catalog/review/etwsrn_send_email_notification_enabled', $storeId);
    }

    /**
     * Check if notifications in admin are enabled
     *
     * @param int|null $storeId
     * @return bool
     */
    public function isNeedShowNotification($storeId = null)
    {
        return (bool)$this->getConfig('catalog/review/etwsrn_show_notification', $storeId);
    }


    /**
     * Retrieve recipients for specified store id
     *
     * @param int|null $storeId
     * @return array|false
     */
    public function getNotificationRecipientEmails($storeId = null)
    {

        $recipients = trim($this->getConfig('catalog/review/etwsrn_mail_to', $storeId));
        if (!empty($recipients)) {
            $recipients = explode(',', $recipients);
            $recipientList = [];
            foreach ($recipients as $recipient) {
                $recipient = trim($recipient);
                if (\Zend_Validate::is($recipient, 'EmailAddress')) {
                    $recipientList[] = $recipient;
                } else {
                    $this->log('Invalid notification email address found. Email: "'
                        . $recipient . '" WebsiteId: "' . $storeId . '"');
                }
            }
            if (!empty($recipientList)) {
                return $recipientList;
            }
        }
        return false;
    }

    /**
     * Retrieve email address to send notifications from for specified store id
     *
     * @param int|null $storeId
     * @return array|false
     */
    public function getNotificationFromEmail($storeId = null)
    {
        $emailIdentityType = $this->getConfig('catalog/review/etwsrn_email_identity', $storeId);
        $email = $this->getConfig('trans_email/ident_' . $emailIdentityType . '/email', $storeId);
        if (\Zend_Validate::is($email, 'EmailAddress')) {
            $emailFrom = array(
                'email' => $email,
                'name' => $this->getConfig('trans_email/ident_' . $emailIdentityType . '/name', $storeId)
            );
            return $emailFrom;
        }
        return false;
    }

    /**
     * Retrieve id of notification template for specified store id
     *
     * @param null $storeId
     * @return int|string|false
     */
    public function getNotificationEmailTemplateId($storeId = null)
    {
        $config = $this->getConfig('catalog/review/etwsrn_email_template', $storeId);
        if ($config !== null) {
            return $config;
        }
        return false;
    }

    /**
     * @inheritdoc
     * @return bool
     */
    public function isLogEnabled()
    {
        return (bool)$this->getConfig('catalog/review/etwsrn_log_enabled');
    }

    /**
     * @inheritdoc
     * @return string
     */
    protected function _getConfigSectionId()
    {
        return 'etws_reviewnotify';
    }

}