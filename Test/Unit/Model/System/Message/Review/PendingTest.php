<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not give, sell, distribute, sub-license, rent, lease or lend
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category  ETWS
 * @package   ETWS_ReviewNotify
 * @copyright Copyright (c) 2017 ET Web Solutions (http://etwebsolutions.com)
 * @contacts  support@etwebsolutions.com
 * @license  https://shop.etwebsolutions.com/etws-license-free-v1/   ETWS Free License (EFL1)
 */

namespace ETWS\ReviewNotify\Test\Unit\Model\System\Message\Review;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Magento\Review\Model\ResourceModel\Review\Collection;
use Magento\Review\Model\Review;
use Magento\Review\Model\ResourceModel\Review\CollectionFactory;
use Magento\Catalog\Model\ProductRepository;
use Magento\Catalog\Model\Product;



/**
 * Class PendingTest
 * @package ETWS\ReviewNotify\Test\Unit\Model\System\Message\Review
 */
class PendingTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \ETWS\ReviewNotify\Model\System\Message\Review\Pending
     */
    protected $pendingModel;

    /**
     * @var ObjectManager
     */
    protected $objectManager;

    /**
     * @var \ETWS\ReviewNotify\Helper\Data
     */
    protected $helperMock;

    /**
     * @var \Magento\Review\Model\ResourceModel\Review\Collection|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $collection;

    /**
     * @var \Magento\Review\Model\ResourceModel\Review\CollectionFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $collectionFactory;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $productRepository;

    /**
     * Prepare test objects
     */
    protected function setUp()
    {
        $this->objectManager = new ObjectManager($this);
        $this->initHelperMock();
        $this->initCollectionMocks();
        $this->initProductMocks();


        $this->pendingModel = $this->objectManager->getObject('\ETWS\ReviewNotify\Model\System\Message\Review\Pending',
            [
                'ratingFactory' => $this->collectionFactory,
                'productRepository' => $this->productRepository,
                'etwsHelper' => $this->helperMock
            ]);
    }


    /**
     * @covers \ETWS\ReviewNotify\Model\System\Message\Review\Pending::isDisplayed
     */
    public function testIsDisplayed()
    {
        // notification enabled and 3 reviews
        $this->assertTrue($this->pendingModel->isDisplayed());
        // notification disabled and 3 reviews
        $this->assertFalse($this->pendingModel->isDisplayed());
        // notification enabled and 1 review
        $this->assertTrue($this->pendingModel->isDisplayed());
        // notification disabled and 1 review
        $this->assertFalse($this->pendingModel->isDisplayed());
        // notification enabled and 0 reviews
        $this->assertFalse($this->pendingModel->isDisplayed());
        // notification disabled and 0 reviews
        $this->assertFalse($this->pendingModel->isDisplayed());
    }

    public function testGetText()
    {
        // 3 reviews
        $this->assertStringStartsWith('There are', $this->pendingModel->getText()->getText());
        // 1 review
        $this->assertStringStartsWith('There is', $this->pendingModel->getText()->getText());
        // 0 reviews
        $this->assertEmpty($this->pendingModel->getText());
    }

    /**
     * Create mock for helper
     * and prepare test values
     */
    protected function initHelperMock()
    {
        $this->helperMock = $this->getMockBuilder('\ETWS\ReviewNotify\Helper\Data')
            ->disableOriginalConstructor()
            ->getMock();

        $this->helperMock->method('isNeedShowNotification')->willReturnOnConsecutiveCalls(true, false, true, false, true, false);
    }

    /**
     * Create mocks for collection and its factory
     */
    protected function initCollectionMocks()
    {
        $this->collection = $this->getMockBuilder(Collection::class)
            ->disableOriginalConstructor()
            ->setMethods(['addStoreData', 'addFieldToFilter', 'getSize', 'getFirstItem', '__wakeup'])
            ->getMock();

        $this->collection->expects(static::any())
            ->method('addStoreData')
            ->willReturnSelf();

        $this->collection->expects(static::any())
            ->method('addFieldToFilter')
            ->willReturnSelf();

        $this->collection->expects(static::any())
            ->method('getSize')
            ->willReturnOnConsecutiveCalls(3, 1, 0, 3, 1, 0);

        $review = $this->getMockBuilder(Review::class)
            ->disableOriginalConstructor()
            ->setMethods(['getEntityPkValue', 'getId', 'getTitle'])
            ->getMock();

        $review->expects(static::any())
            ->method('getEntityPkValue')
            ->willReturn(15);

        $review->expects(static::any())
            ->method('getId')
            ->willReturn(22);

        $review->expects(static::any())
            ->method('getTitle')
            ->willReturn('Test Title');

        $this->collection->expects(static::any())
            ->method('getFirstItem')
            ->willReturn($review);

        $this->collectionFactory = $this->getMockBuilder(CollectionFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(['create', '__wakeup'])
            ->getMock();

        $this->collectionFactory->expects(static::once())
            ->method('create')
            ->willReturn($this->collection);
    }

    /**
     * Create mocks for reviewed product
     */
    protected function initProductMocks()
    {
        $product = $this->getMockBuilder(Product::class)
            ->disableOriginalConstructor()
            ->setMethods(['getId'])
            ->getMock();

        $this->collectionFactory->expects(static::any())
            ->method('getId')
            ->willReturn(15);

        $this->productRepository = $this->getMockBuilder(ProductRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(['getById'])
            ->getMock();

        $this->productRepository->expects(static::any())
            ->method('getById')
            ->willReturn($product);
    }

}