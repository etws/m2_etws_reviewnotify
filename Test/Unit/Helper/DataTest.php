<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not give, sell, distribute, sub-license, rent, lease or lend
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category  ETWS
 * @package   ETWS_ReviewNotify
 * @copyright Copyright (c) 2017 ET Web Solutions (http://etwebsolutions.com)
 * @contacts  support@etwebsolutions.com
 * @license  https://shop.etwebsolutions.com/etws-license-free-v1/   ETWS Free License (EFL1)
 */


namespace ETWS\ReviewNotify\Test\Unit\Helper;

use ETWS\ReviewNotify\Test\MockMap;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Magento\Store\Model\ScopeInterface;

/**
 * Class DataTest
 * @package ETWS\ReviewNotify\Test\Unit\Helper
 */
class DataTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \ETWS\ReviewNotify\Helper\Data
     */
    protected $dataHelper;

    /**
     * @var \ETWS\Base\Logger\Logger
     */
    protected $loggerMock;

    /**
     * @var \Magento\Framework\App\Helper\Context
     */
    protected $context;

    /**
     * @var ObjectManager
     */
    protected $objectManager;

    /**
     * @covers \ETWS\ReviewNotify\Helper\Data::isNeedSendNotification
     */
    public function testIsNeedSendNotification()
    {
        $this->assertTrue($this->dataHelper->isNeedSendNotification());

    }

    /**
     * @covers \ETWS\ReviewNotify\Helper\Data::isNeedShowNotification
     */
    public function testIsNeedShowNotification()
    {
        $this->assertFalse($this->dataHelper->isNeedShowNotification());
    }

    /**
     * @covers \ETWS\ReviewNotify\Helper\Data::getNotificationRecipientEmails
     */
    public function testGetNotificationRecipientEmails()
    {
        $this->assertFalse($this->dataHelper->getNotificationRecipientEmails());
        $this->assertCount(1, $this->dataHelper->getNotificationRecipientEmails());
        $this->assertCount(3, $this->dataHelper->getNotificationRecipientEmails());

        $caseNr4 = $this->dataHelper->getNotificationRecipientEmails();
        $this->assertCount(1, $caseNr4);
        $this->assertContains('example@example.com', $caseNr4);

        $this->assertCount(1, $this->dataHelper->getNotificationRecipientEmails());
    }

    /**
     * @covers \ETWS\ReviewNotify\Helper\Data::getNotificationFromEmail
     */
    public function testGetNotificationFromEmail()
    {
        $case1 = $this->dataHelper->getNotificationFromEmail();
        $this->assertCount(2, $case1);
        $this->assertArrayHasKey('email', $case1);
        $this->assertContains('test_example@example.com', $case1);
    }

    /**
     * @covers \ETWS\ReviewNotify\Helper\Data::getNotificationEmailTemplateId
     */
    public function testGetNotificationEmailTemplateId()
    {
        $this->assertEquals('catalog_review_etwsrn_email_template', $this->dataHelper->getNotificationEmailTemplateId());
    }

    /**
     * Prepare test objects
     *
     */
    protected function setUp()
    {
        $this->objectManager = new ObjectManager($this);

        $this->initLoggerMock();
        $this->initContextMock();

        $this->dataHelper = $this->objectManager->getObject('\ETWS\ReviewNotify\Helper\Data', [
            'context' => $this->context,
            'logger' => $this->loggerMock
        ]);
    }

    /**
     * Create mock for logger
     */
    protected function initLoggerMock()
    {
        $this->loggerMock = $this->getMock('\ETWS\Base\Logger\Logger', [], ['name' => 'ETWS Logger']);
        $this->loggerMock->method('getHandlers')->willReturn([]);

    }

    /**
     * Create mock for context
     */
    protected function initContextMock()
    {
        $map = $this->getScopeConfigMap();

        $scopeConfigMock = $this->getMock('\Magento\Framework\App\Config\ScopeConfigInterface');
        $mockMap = new MockMap($map);
        $scopeConfigMock->method('getValue')->will($mockMap);

        /** @var \Magento\Framework\App\Helper\Context $context */
        $this->context = $this->objectManager->getObject('Magento\Framework\App\Helper\Context', [
            'scopeConfig' => $scopeConfigMock
        ]);

    }

    /**
     * Prepare configuration values for tests
     *
     * @return array
     */
    protected function getScopeConfigMap()
    {
        $type = ScopeInterface::SCOPE_STORE;
        $prefix = 'catalog/review/';
        $map = [
            [$prefix . 'etwsrn_send_email_notification_enabled', $type, null, true],
            [$prefix . 'etwsrn_show_notification', $type, null, false],
            [$prefix . 'etwsrn_mail_to', $type, null,
                [
                    '', // None entered
                    'example@example.com', // 1 correct mail
                    'example@example.com,example2@example.com,   example3@example.com', // 3 correct mails with space
                    'ex,@,aa@aa, example@example.com', // 1 correct mail
                    'ex,@,aa@aa , , example@example.com' // 1 correct mail
                ]
            ],
            [$prefix . 'etwsrn_email_template', $type, null, 'catalog_review_etwsrn_email_template'], //Any string at all
            [$prefix . 'etwsrn_email_identity', $type, null, 'general'],
            ['trans_email/ident_general/email', $type, null, 'test_example@example.com'],
            ['trans_email/ident_general/name', $type, null, 'Test Owner'],
        ];

        return $map;
    }
}
