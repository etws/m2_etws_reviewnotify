This file describes changes between versions of module M2_ReviewNotify for Magento 2.

Legend:
* bug fix
+ added functionality
- removed functionality

TODO:

=====================================

1.0.1 (10/04/2017)
* fixed inability to post reviews with full page caching and spambot protection enabled

1.0.0 (28/02/2017)
+ first public release
