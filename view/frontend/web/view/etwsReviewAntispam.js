/*
 * NOTICE OF LICENSE
 *
 * You may not give, sell, distribute, sub-license, rent, lease or lend
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category  ETWS
 * @package   ETWS_ReviewNotify
 * @copyright Copyright (c) 2017 ET Web Solutions (http://etwebsolutions.com)
 * @contacts  support@etwebsolutions.com
 * @license  https://shop.etwebsolutions.com/etws-license-free-v1/   ETWS Free License (EFL1)
 */

define([
    'jquery',
    'jquery/ui',
    'mage/validation/validation'
], function ($) {
    'use strict';

    $.widget('mage.etwsReviewAntispam', {
        options: {
            hashField: {
                id: "sequence_field",
                name: "sequence"
            },
            prePostUrl: false
        },
        _create: function () {
            this._bind();
        },
        _bind: function () {
            this._on({
                submit: '_submit'
            })
        },
        _submit: function () {
            var self = this,
                form = this.element,
                options = this.options;
            if (!form.validation('isValid') || !options.prePostUrl) {
                return true;
            }
            if (!this._hasHash()) {
                $.ajax({
                    url: options.prePostUrl,
                    data: form.serialize(),
                    type: 'post',
                    dataType: 'json',
                    beforeSend: function () {
                        form.find('button[type="submit"]').attr('disabled', 'disabled');
                    },
                    success: function (res) {
                        self._getHashField().val(res.sequence);
                        form.find('button[type="submit"]').attr('disabled', null);
                        form.submit();
                    }
                });
                return false;
            }
        },
        _getHashField: function () {
            var form = this.element,
                config = this.options.hashField,
                field = form.find('#' + config.id);
            if (!field.length) {
                field = $('<input>');
                field.attr({
                    type: 'hidden',
                    id: config.id,
                    name: config.name
                });
                form.append(field);

            }
            return field.eq(0);

        },
        _hasHash: function () {
            var field = this._getHashField();
            return Boolean(field.val());
        }
    });

    return $.mage.etwsReviewAntispam;
});